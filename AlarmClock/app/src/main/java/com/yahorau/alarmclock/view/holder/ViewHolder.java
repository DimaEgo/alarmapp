package com.yahorau.alarmclock.view.holder;

import android.support.v7.widget.RecyclerView;
import android.view.View;
import android.widget.ImageView;
import android.widget.Switch;
import android.widget.TextView;
import com.yahorau.alarmclock.R;
import butterknife.Bind;
import butterknife.ButterKnife;

public class ViewHolder extends RecyclerView.ViewHolder{

    @Bind(R.id.tv_time)
    public TextView tvTime;
    @Bind(R.id.tv_date)
    public TextView tvDate;
    @Bind(R.id.tv_description)
    public TextView tvDescription;
    @Bind(R.id.tv_day_of_week)
    public TextView tvDayOfWeek;
    @Bind(R.id.sw_alarm_on_off)
    public Switch swAlarmOnOff;
    @Bind(R.id.iv_unlock_mode)
    public ImageView ivTypeAlarm;
    @Bind(R.id.iv_difficulty)
    public ImageView ivDifficulty;

    public ViewHolder(View itemView) {
        super(itemView);
        ButterKnife.bind(this, itemView);
    }
}