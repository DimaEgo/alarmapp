package com.yahorau.alarmclock.view.interfaces;

import com.yahorau.alarmclock.model.Alarm;

public interface IMainPresenter {
    void onAlarmAdded(Alarm alarm);
}
