package com.yahorau.alarmclock.presenter;

import android.app.DatePickerDialog;
import android.app.TimePickerDialog;
import android.content.Context;
import android.content.Intent;
import android.widget.DatePicker;
import android.widget.TimePicker;

import com.yahorau.alarmclock.R;
import com.yahorau.alarmclock.model.Alarm;
import com.yahorau.alarmclock.model.AlarmService;
import com.yahorau.alarmclock.model.DbHelper;
import com.yahorau.alarmclock.model.enums.AlarmDifficulty;
import com.yahorau.alarmclock.model.enums.AlarmType;
import com.yahorau.alarmclock.view.dialogs.AlarmOptionsFragment;
import com.yahorau.alarmclock.view.interfaces.IAlarmView;

import java.util.Calendar;

import static java.util.Calendar.DATE;
import static java.util.Calendar.MONTH;
import static java.util.Calendar.YEAR;

public class AlarmPresenter implements DatePickerDialog.OnDateSetListener,
        TimePickerDialog.OnTimeSetListener, AlarmOptionsFragment.IAlarmSettings {
    private final Context context;
    private final IAlarmView alarmView;
    private final Alarm alarm;

    public AlarmPresenter(IAlarmView alarmView, Context context, Alarm alarm) {
        this.alarmView = alarmView;
        this.context = context;
        this.alarm = alarm;
    }

    /**
     * Метод вызывается после установки даты в DatePicker
     * после чего обновляется ui DatePicker
     */
    @Override
    public void onDateSet(DatePicker datePicker, int i, int i1, int i2) {
        alarm.getDate().set(YEAR, i);
        alarm.getDate().set(MONTH, i1);
        alarm.getDate().set(DATE, i2);

        if (alarmView != null) {
            alarmView.onDateChanged(alarm.getDate());
        }
    }

    /**
     * Метод вызывается после установки
     * настроек в AlarmOptionsFragment
     */
    @Override
    public void onSettingsChanged(int item, int item2) {
        if (item < AlarmType.values().length){
            alarm.setAlarmType(AlarmType.values()[item]);
        }

        if (item2 < AlarmDifficulty.values().length) {
            alarm.setAlarmDifficulty(AlarmDifficulty.values()[item2]);
        }
    }

    /**
     * Метод вызывается после установки даты в TimePicker
     * после чего обновляется ui TimePicker
     */
    @Override
    public void onTimeSet(TimePicker timePicker, int i, int i1) {
        alarm.getDate().set(Calendar.HOUR_OF_DAY, i);
        alarm.getDate().set(Calendar.MINUTE, i1);

        if (alarmView != null) {
            alarmView.onTimeChanged(alarm.getDate());
        }
    }

    public void updateDescription(String description){
        alarm.setDescription(description);
    }

    /**
     * Сохранение будильника
     */
    public void save(){
        // Проверяется валидность выбронной даты
        if (alarm.getDate().getTimeInMillis() > System.currentTimeMillis()) {
            saveToDataBase();

            // запускаем Alarm Service
            startNotificationService(context);
            alarmView.onSave(alarm);
        } else {
            alarmView.onError(context.getString(R.string.error_time));
        }
    }

    private void saveToDataBase() {
        DbHelper.init(context);

        if (alarm.getId() < 0) {
            DbHelper.create(alarm);
        } else {
            DbHelper.update(alarm);
        }
    }

    private void startNotificationService(Context context) {
        Intent serviceIntent = new Intent(context, AlarmService.class);
        context.startService(serviceIntent);
        context.sendBroadcast(serviceIntent, null);
    }

    @Override
    public AlarmType getCurrentType() {
        return alarm.getAlarmType();
    }

    @Override
    public AlarmDifficulty getCurrentDifficulty() {
        return alarm.getAlarmDifficulty();
    }


    public Alarm getAlarm() {
        return alarm;
    }
}
