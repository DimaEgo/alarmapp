package com.yahorau.alarmclock.model.enums;

public enum AlarmDifficulty {
    Lite,
    Medium,
    Strong
}
