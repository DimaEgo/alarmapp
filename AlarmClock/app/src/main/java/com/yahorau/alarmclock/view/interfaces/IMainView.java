package com.yahorau.alarmclock.view.interfaces;


import com.yahorau.alarmclock.model.Alarm;

public interface IMainView {
    void onAlarmChanged(String message);
    void showEmptyContentScreen();
    void showNewAlarmWith(Alarm alarm);
}
