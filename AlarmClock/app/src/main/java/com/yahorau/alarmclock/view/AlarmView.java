package com.yahorau.alarmclock.view;

import android.os.Bundle;
import android.support.annotation.Nullable;
import android.support.v4.app.Fragment;
import android.support.v4.app.FragmentManager;
import android.support.v7.app.ActionBar;
import android.support.v7.app.AppCompatActivity;
import android.text.TextUtils;
import android.view.KeyEvent;
import android.view.LayoutInflater;
import android.view.MotionEvent;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ArrayAdapter;
import android.widget.Button;
import android.widget.EditText;
import android.widget.Spinner;
import android.widget.Switch;
import android.widget.Toast;
import com.yahorau.alarmclock.R;
import com.yahorau.alarmclock.model.Alarm;
import com.yahorau.alarmclock.presenter.AlarmPresenter;
import com.yahorau.alarmclock.view.dialogs.AlarmOptionsFragment;
import com.yahorau.alarmclock.view.dialogs.DatePickerFragment;
import com.yahorau.alarmclock.view.dialogs.TimePickerFragment;
import com.yahorau.alarmclock.view.interfaces.IMainPresenter;
import com.yahorau.alarmclock.view.interfaces.IAlarmView;
import com.yahorau.alarmclock.view.util.CalendarHelper;
import java.util.ArrayList;
import java.util.Calendar;
import butterknife.Bind;
import butterknife.ButterKnife;
import butterknife.OnClick;

public class AlarmView extends Fragment implements IAlarmView {

    public static final String TAG_TIME_DIALOG = "Time";
    public static final String TAG_DATE_DIALOG = "Date";
    public static final String DATES = "Dates";
    public static final String TIMES = "Times";

    @Bind(R.id.et_description)
    EditText etDescription;
    @Bind(R.id.sp_date)
    Spinner spDate;
    @Bind(R.id.sp_time)
    Spinner spTime;
    @Bind(R.id.sw_repeat_mode_on)
    Switch swActivated;
    @Bind(R.id.btn_save)
    Button btnSave;

    private ArrayList<String> dates;
    private ArrayAdapter<String> adapterDate;
    private ArrayList<String> times;
    private ArrayAdapter<String> adapterTime;
    private IMainPresenter mainPresenter;
    private AlarmPresenter presenter;
    private Alarm alarm;
    private ActionBar supportActionBar;

    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setRetainInstance(true);
    }

    @Nullable
    @Override
    public View onCreateView(LayoutInflater inflater, @Nullable ViewGroup container, @Nullable Bundle savedInstanceState) {
        View v = inflater.inflate(R.layout.new_alarm_screen, container, false);
        ButterKnife.bind(this, v);
        return v;
    }

    @Override
    public void onActivityCreated(@Nullable Bundle savedInstanceState) {
        super.onActivityCreated(savedInstanceState);

        supportActionBar = ((AppCompatActivity) getActivity()).getSupportActionBar();
        if (supportActionBar != null) {
            supportActionBar.setDisplayHomeAsUpEnabled(false);
            supportActionBar.setLogo(R.drawable.toolbar_logo);
            supportActionBar.setTitle(getString(R.string.toolbar_title_new_alarm));
        }
        presenter = new AlarmPresenter(this, getContext(), alarm);
        createSpinnerListeners();

        setupSpinnersContent(savedInstanceState);
        createTimeAdapter();
        createDateAdapter();
    }

    private void setupSpinnersContent(@Nullable Bundle savedInstanceState) {
        if (savedInstanceState == null) {
            dates = new ArrayList<>();
            times = new ArrayList<>();
        } else {
            dates = savedInstanceState.getStringArrayList(DATES);
            times = savedInstanceState.getStringArrayList(TIMES);
        }
    }

    private void createDateAdapter() {
        dates.add(CalendarHelper.getAlarmDatePickerString(presenter.getAlarm().getDate()));
        adapterDate = new ArrayAdapter<>(getContext(), android.R.layout.simple_spinner_item, dates);
        spDate.setAdapter(adapterDate);
    }

    private void createTimeAdapter() {
        times.add(CalendarHelper.getAlarmTimeString(presenter.getAlarm().getDate()));
        adapterTime = new ArrayAdapter<>(getContext(), android.R.layout.simple_spinner_item, times);
        spTime.setAdapter(adapterTime);
    }

    private boolean validateEditText(String fieldValue){
        boolean isValid = !TextUtils.isEmpty(fieldValue);
        if (isValid){
            etDescription.setError(null);
        } else {
            etDescription.setError(getString(R.string.empty_field));
        }

        return isValid;
    }

    @Override
    public void onSaveInstanceState(Bundle outState) {
        super.onSaveInstanceState(outState);
        outState.putStringArrayList(DATES, dates);
        outState.putStringArrayList(TIMES, times);
    }

    @Override
    public void onResume() {
        super.onResume();
        if (supportActionBar != null) {
            supportActionBar.setTitle(getString(R.string.toolbar_title_new_alarm));
            supportActionBar.setLogo(R.drawable.toolbar_logo);
        }
    }

    private void createSpinnerListeners() {
        spDate.setOnTouchListener(Spinner_OnTouch);
        spDate.setOnKeyListener(Spinner_OnKey);
        spTime.setOnTouchListener(Spinner_OnTouch);
        spTime.setOnKeyListener(Spinner_OnKey);
    }

    @Override
    public void onTimeChanged(Calendar time) {
        times.clear();
        times.add(CalendarHelper.getAlarmTimeString(time));
        adapterTime.notifyDataSetChanged();
    }

    @Override
    public void onDateChanged(Calendar date) {
        dates.clear();
        dates.add(CalendarHelper.getAlarmDatePickerString(date));
        adapterDate.notifyDataSetChanged();
    }

    public void showDatePicker() {
        DatePickerFragment datePicker = new DatePickerFragment();
        datePicker.setCalendar(presenter.getAlarm().getDate());
        datePicker.setListener(presenter);
        datePicker.show(getActivity().getSupportFragmentManager(), TAG_DATE_DIALOG);
    }

    public void showTimePicker() {
        TimePickerFragment timePicker = new TimePickerFragment();
        timePicker.setDate(presenter.getAlarm().getDate());
        timePicker.setListener(presenter);
        timePicker.show(getActivity().getSupportFragmentManager(), TAG_TIME_DIALOG);
    }

    @OnClick(R.id.layout_type)
    public void showDifficulty() {
        AlarmOptionsFragment fragment = new AlarmOptionsFragment();
        fragment.setListener(presenter);

        FragmentManager fragmentManager = getActivity().getSupportFragmentManager();
        fragmentManager
                .beginTransaction()
                .replace(R.id.content_main, fragment)
                .addToBackStack(null)
                .commit();
    }

    @Override
    public void onSave(Alarm alarm) {
        getActivity().getSupportFragmentManager().popBackStack();
        mainPresenter.onAlarmAdded(alarm);
    }

    @Override
    public void onError(String message) {
        Toast.makeText(getContext(), message, Toast.LENGTH_LONG).show();
    }

    @OnClick(R.id.btn_save)
    public void createAlarm() {
        if (validateEditText(etDescription.getText().toString())) {
            presenter.updateDescription(etDescription.getText().toString());
            presenter.save();
        }
    }

    View.OnTouchListener Spinner_OnTouch = new View.OnTouchListener() {
        public boolean onTouch(View v, MotionEvent event) {
            if (event.getAction() == MotionEvent.ACTION_UP) {
                showPicker(v);
            }
            return true;
        }
    };

    View.OnKeyListener Spinner_OnKey = new View.OnKeyListener() {
        public boolean onKey(View v, int keyCode, KeyEvent event) {
            if (keyCode == KeyEvent.KEYCODE_DPAD_CENTER) {
                showPicker(v);
                return true;
            } else {
                return false;
            }
        }
    };

    private void showPicker(View v) {
        switch (v.getId()) {
            case (R.id.sp_date):
                showDatePicker();
                break;
            case (R.id.sp_time):
                showTimePicker();
                break;
            default:
                break;
        }
    }

    public void setMainPresenter(IMainPresenter mainPresenter) {
        this.mainPresenter = mainPresenter;
    }

    public void setAlarm(Alarm alarm) {
        this.alarm = alarm;
    }
}
