package com.yahorau.alarmclock.presenter;

import android.app.Notification;
import android.app.NotificationManager;
import android.app.PendingIntent;
import android.content.BroadcastReceiver;
import android.content.Context;
import android.content.Intent;
import android.media.RingtoneManager;
import android.os.Bundle;

import com.yahorau.alarmclock.R;
import com.yahorau.alarmclock.model.Alarm;
import com.yahorau.alarmclock.view.MainActivity;

import static android.content.Context.NOTIFICATION_SERVICE;

public class NotificationReceiver extends BroadcastReceiver {

    @Override
    public void onReceive(Context context, Intent intent) {

        Intent startActivityIntent = new Intent(context, MainActivity.class);
        startActivityIntent.addFlags(Intent.FLAG_ACTIVITY_NEW_TASK);

        PendingIntent pIntent = PendingIntent.getActivity(context, 0, startActivityIntent, 0);

        Bundle bundle = intent.getExtras();
        Alarm alarm = (Alarm) bundle.getSerializable("alarm");

        Notification n  = new Notification.Builder(context)
                .setContentTitle(alarm.getDescription())
                .setSmallIcon(R.drawable.ic_alarm_48dp)
                .setContentIntent(pIntent)
                .setVibrate(new long[] { 0, 200, 200 })
                .setSound(RingtoneManager.getDefaultUri(RingtoneManager.TYPE_NOTIFICATION))
                .setAutoCancel(true)
                .build();

        NotificationManager notificationManager =
                (NotificationManager) context.getSystemService(NOTIFICATION_SERVICE);

        notificationManager.notify(0, n);
        context.startActivity(startActivityIntent);
    }
}
