package com.yahorau.alarmclock.model;

import android.support.v4.content.ContextCompat;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.CompoundButton;

import com.yahorau.alarmclock.R;
import com.yahorau.alarmclock.model.enums.AlarmDifficulty;
import com.yahorau.alarmclock.model.enums.AlarmType;
import com.yahorau.alarmclock.view.holder.ViewHolder;
import com.yahorau.alarmclock.view.util.CalendarHelper;

import java.util.ArrayList;
import java.util.Calendar;

public class AlarmAdapter extends RecyclerView.Adapter<ViewHolder> {

    private final IAlarmAdapter listener;
    private ArrayList<Alarm> items;

    public AlarmAdapter(ArrayList<Alarm> items, IAlarmAdapter listener) {
        super();
        this.listener = listener;
        this.items = items;
    }

    @Override
    public ViewHolder onCreateViewHolder(ViewGroup parent, int viewType) {
        final int layout = R.layout.item_alarm;
        View v = LayoutInflater.from(parent.getContext()).inflate(layout, parent, false);

        return new ViewHolder(v);
    }

    @Override
    public void onBindViewHolder(final ViewHolder holder, int position) {
        Alarm alarm = items.get(holder.getAdapterPosition());
        Calendar alarmDate = alarm.getDate();

        holder.tvDescription.setText(alarm.getDescription());
        holder.tvDate.setText(CalendarHelper.getAlarmDateString(alarmDate));
        holder.tvTime.setText(CalendarHelper.getAlarmTimeString(alarmDate));
        holder.tvDayOfWeek.setText(CalendarHelper.getDayOfWeek(alarmDate));
        holder.ivDifficulty.setImageDrawable(ContextCompat.getDrawable(holder.itemView.getContext(), getDifficultyBackground(alarm.getAlarmDifficulty())));
        holder.ivTypeAlarm.setImageDrawable(ContextCompat.getDrawable(holder.itemView.getContext(), getTypeAlarmBackground(alarm.getAlarmType())));
        holder.swAlarmOnOff.setChecked(alarm.isActivated());

        holder.swAlarmOnOff.setOnCheckedChangeListener(new CompoundButton.OnCheckedChangeListener() {
            @Override
            public void onCheckedChanged(CompoundButton compoundButton, boolean b) {
                if (listener != null){
                    listener.onChangeActive(holder.getAdapterPosition(), b);
                }
            }
        });
        holder.itemView.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                if (listener != null){
                    listener.onAlarmItemClicked(holder.getAdapterPosition());
                }
            }
        });
    }

    private int getDifficultyBackground(AlarmDifficulty difficulty) {
        switch (difficulty) {
            case Lite:
                return R.drawable.ic_lite;
            case Medium:
                return R.drawable.ic_medium;
            case Strong:
                return R.drawable.ic_strong;
            default:
                return 0;
        }
    }

    private int getTypeAlarmBackground(AlarmType alarmType) {
        switch (alarmType) {
            case Plus:
                return R.drawable.ic_plus;
            case Points:
                return R.drawable.ic_points;
            case PlusAndPosints:
                return R.drawable.ic_plus_and_points;
            default:
                return 0;
        }
    }

    @Override
    public int getItemCount() {
        return items.size();
    }

    public interface IAlarmAdapter {
        void onChangeActive(int position, boolean b);
        void onAlarmItemClicked(int position);
    }
}
