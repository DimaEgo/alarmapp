package com.yahorau.alarmclock.view.interfaces;


import com.yahorau.alarmclock.model.Alarm;

import java.util.Calendar;

public interface IAlarmView {
    void onSave(Alarm alarm);
    void onError(String message);
    void onTimeChanged(Calendar time);
    void onDateChanged(Calendar date);
}
