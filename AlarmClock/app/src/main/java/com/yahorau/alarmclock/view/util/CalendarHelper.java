package com.yahorau.alarmclock.view.util;

import java.text.DateFormat;
import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.Calendar;
import java.util.Date;
import java.util.Locale;

public class CalendarHelper {

    public static String getAlarmDateString(Calendar calendar) {
        SimpleDateFormat monthParse = new SimpleDateFormat("MM", Locale.getDefault());
        SimpleDateFormat monthDisplay = new SimpleDateFormat("MMM", Locale.getDefault());
        String formattedString = "";
        try {
            formattedString = calendar.get(Calendar.DATE)
                    + " " + monthDisplay.format(monthParse.parse(String.valueOf(calendar.get(Calendar.MONTH) + 1)))
                    + " " + calendar.get(Calendar.YEAR);
        } catch (ParseException e) {
            e.printStackTrace();
        }

        return formattedString;
    }

    public static String getAlarmDatePickerString(Calendar calendar) {
        SimpleDateFormat weekDay = new SimpleDateFormat("MM", Locale.getDefault());
        SimpleDateFormat monthParse = new SimpleDateFormat("MM", Locale.getDefault());
        SimpleDateFormat monthDisplay = new SimpleDateFormat("MMM", Locale.getDefault());
        String formattedString = "";
        try {
            formattedString = String.format(Locale.getDefault(), getDayOfWeek(calendar), weekDay)
                    + ", " + monthDisplay.format(monthParse.parse(String.valueOf(calendar.get(Calendar.MONTH) + 1)))
                    + " " + calendar.get(Calendar.DATE)
                    + ", " + calendar.get(Calendar.YEAR);
        } catch (ParseException e) {
            e.printStackTrace();
        }

        return formattedString;
    }

    public static String getAlarmTimeString(Calendar calendar) {
        return String.format(Locale.getDefault(),"%2d:%02d", calendar.get(Calendar.HOUR_OF_DAY), calendar.get(Calendar.MINUTE));
    }

    public static String getDayOfWeek(Calendar calendar) {
        String input_date = String.valueOf(calendar.get(Calendar.DATE)) + "/" +
                String.valueOf(calendar.get(Calendar.MONTH) + 1) + "/" +
                String.valueOf(calendar.get(Calendar.YEAR));

        SimpleDateFormat format1 = new SimpleDateFormat("dd/MM/yyyy", Locale.getDefault());
        Date date = null;
        try {
            date = format1.parse(input_date);
        } catch (ParseException e) {
            e.printStackTrace();
        }
        DateFormat format2 = new SimpleDateFormat("EEEE", Locale.getDefault());

        return format2.format(date);
    }

    public static String getTimeUntilNextAlarmMessage(Calendar date) {
        long timeDifference = date.getTimeInMillis() - System.currentTimeMillis();
        long days = timeDifference / (1000 * 60 * 60 * 24);
        long hours = timeDifference / (1000 * 60 * 60) - (days * 24);
        long minutes = timeDifference / (1000 * 60) - (days * 24 * 60) - (hours * 60);
        long seconds = timeDifference / (1000) - (days * 24 * 60 * 60) - (hours * 60 * 60) - (minutes * 60);
        String alert = "Будильник включится через ";
        if (days > 0) {
            alert += String.format(Locale.getDefault(),
                    "%d дней, %d часов, %d минут and %d секунд", days,
                    hours, minutes, seconds);
        } else {
            if (hours > 0) {
                alert += String.format(Locale.getDefault(), "%d дней, %d минут and %d секунд",
                        hours, minutes, seconds);
            } else {
                if (minutes > 0) {
                    alert += String.format(Locale.getDefault(), "%d минут, %d секунд", minutes,
                            seconds);
                } else {
                    alert += String.format(Locale.getDefault(), "%d секунд", seconds);
                }
            }
        }
        return alert;
    }
}
