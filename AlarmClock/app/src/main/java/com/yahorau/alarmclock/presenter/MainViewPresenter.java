package com.yahorau.alarmclock.presenter;

import android.content.Context;

import com.yahorau.alarmclock.model.AlarmAdapter;
import com.yahorau.alarmclock.model.Alarm;
import com.yahorau.alarmclock.model.AlarmService;
import com.yahorau.alarmclock.model.DbHelper;
import com.yahorau.alarmclock.view.interfaces.IMainPresenter;
import com.yahorau.alarmclock.view.interfaces.IMainView;
import com.yahorau.alarmclock.view.util.CalendarHelper;

import java.util.ArrayList;

public class MainViewPresenter implements IMainPresenter, AlarmAdapter.IAlarmAdapter {
    private final Context context;
    private final IMainView mView;
    private final AlarmAdapter mAlarmAdapter;
    private ArrayList<Alarm> itemAlarms;

    public MainViewPresenter(IMainView mView, Context context) {
        this.mView = mView;
        this.context = context;
        this.itemAlarms = new ArrayList<>();

        DbHelper.init(context);
        itemAlarms.addAll(DbHelper.getAll());

        this.mAlarmAdapter = new AlarmAdapter(itemAlarms, this);
    }

    /**
     * Метод реагирут на переключение switch
     * Если будильник проходит простую валидатацию и switch isChecked,
     * то он будильник добавляется в очередь AlarmManager,
     * иначе удаляется из этой очереди
     */
    @Override
    public void onChangeActive(int position, boolean b) {
        Alarm alarm = itemAlarms.get(position);
        alarm.setActivated(b);

        if (alarm.getDate().getTimeInMillis() - System.currentTimeMillis() > 0) {
            if (b){
                AlarmService.add(alarm, context);
                mView.onAlarmChanged(CalendarHelper.getTimeUntilNextAlarmMessage(alarm.getDate()));
            } else {
                AlarmService.cancel(alarm, context);
            }
        }
    }

    /**
     * Переход к редактированию по клику на выбранный будильник
     * @param position
     */
    @Override
    public void onAlarmItemClicked(int position) {
        mView.showNewAlarmWith(itemAlarms.get(position));
    }

    /**
     * Обновление списка будильников после добавления / редактирования
     * @param alarm
     */
    @Override
    public void onAlarmAdded(Alarm alarm) {
        itemAlarms.add(0, alarm);
        mAlarmAdapter.notifyDataSetChanged();
        mView.onAlarmChanged(CalendarHelper.getTimeUntilNextAlarmMessage(alarm.getDate()));
    }

    public boolean isEmpty() {
        return itemAlarms.isEmpty();
    }

    public AlarmAdapter getAlarmAdapter() {
        return mAlarmAdapter;
    }

    public ArrayList<Alarm> getItemAlarms() {
        return itemAlarms;
    }

    public void setItemAlarms(ArrayList<Alarm> itemAlarms) {
        this.itemAlarms = itemAlarms;
    }
}
