package com.yahorau.alarmclock.view;

import android.animation.StateListAnimator;
import android.os.Bundle;
import android.support.annotation.Nullable;
import android.support.design.widget.FloatingActionButton;
import android.support.v4.app.Fragment;
import android.support.v4.app.FragmentManager;
import android.support.v7.app.ActionBar;
import android.support.v7.app.AppCompatActivity;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.RelativeLayout;
import android.widget.Toast;
import com.yahorau.alarmclock.R;
import com.yahorau.alarmclock.model.Alarm;
import com.yahorau.alarmclock.presenter.MainViewPresenter;
import com.yahorau.alarmclock.view.interfaces.IMainView;
import java.util.ArrayList;
import butterknife.Bind;
import butterknife.ButterKnife;
import butterknife.OnClick;

public class MainView extends Fragment implements IMainView{

    private static final String STATE_ITEMS = "alarms";

    @Bind(R.id.recycler_view)
    RecyclerView mRecyclerView;
    @Bind(R.id.fab)
    FloatingActionButton fab;
    @Bind(R.id.layout_empty_main_screen)
    RelativeLayout layoutEmptyScreen;

    private MainViewPresenter presenter;

    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setRetainInstance(true);
    }

    @Nullable
    @Override
    public View onCreateView(LayoutInflater inflater, @Nullable ViewGroup container, @Nullable Bundle savedInstanceState) {
        View view = inflater.inflate(R.layout.main_screen, container, false);
        ButterKnife.bind(this, view);
        return view;
    }

    @Override
    public void onActivityCreated(@Nullable Bundle savedInstanceState) {
        super.onActivityCreated(savedInstanceState);

        ActionBar supportActionBar = ((AppCompatActivity) getActivity()).getSupportActionBar();
        if (supportActionBar != null) {
            supportActionBar.setTitle(getResources().getString(R.string.app_name));
            supportActionBar.setLogo(R.drawable.toolbar_logo);
        }
        presenter = new MainViewPresenter(this, getContext());
        updatePresenterOnRotate(savedInstanceState);
        showEmptyContentScreen();
        setupRecyclerView();
    }

    private void updatePresenterOnRotate(@Nullable Bundle savedInstanceState) {
        if (savedInstanceState != null) {
            presenter.setItemAlarms((ArrayList<Alarm>) savedInstanceState.getSerializable(STATE_ITEMS));
        }
    }

    private void setupRecyclerView() {
        mRecyclerView.setHasFixedSize(true);
        mRecyclerView.setStateListAnimator(new StateListAnimator());
        mRecyclerView.setLayoutManager(new LinearLayoutManager(getContext(), LinearLayoutManager.VERTICAL, false));
        mRecyclerView.setAdapter(presenter.getAlarmAdapter());
    }

    @Override
    public void onSaveInstanceState(Bundle outState) {
        super.onSaveInstanceState(outState);
        outState.putSerializable(STATE_ITEMS, presenter.getItemAlarms());
    }

    @Override
    public void onDestroy() {
        super.onDestroy();
        if (getActivity().isFinishing()) {
            presenter = null;
        }
    }

    @Override
    public void onAlarmChanged(String message){
        Toast.makeText(getContext(), message, Toast.LENGTH_LONG).show();
    }

    @OnClick(R.id.fab)
    public void openNewAlarmDialog() {
        showNewAlarmWith(new Alarm());
    }

    @Override
    public void showEmptyContentScreen() {
        layoutEmptyScreen.setVisibility(presenter.isEmpty() ? View.VISIBLE : View.GONE);
    }

    @Override
    public void showNewAlarmWith(Alarm alarm) {
        AlarmView fragment = new AlarmView();
        fragment.setMainPresenter(presenter);
        fragment.setAlarm(alarm);
        FragmentManager fragmentManager = getActivity().getSupportFragmentManager();
        fragmentManager
                .beginTransaction()
                .replace(R.id.content_main, fragment)
                .addToBackStack(null)
                .commit();
    }
}
