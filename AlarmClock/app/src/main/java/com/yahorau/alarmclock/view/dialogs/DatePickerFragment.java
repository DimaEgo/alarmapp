package com.yahorau.alarmclock.view.dialogs;

import android.app.DatePickerDialog;
import android.app.Dialog;
import android.os.Bundle;
import android.support.annotation.NonNull;
import android.support.v4.app.DialogFragment;
import java.util.Calendar;

public class DatePickerFragment extends DialogFragment {

    private DatePickerDialog.OnDateSetListener mListener;
    private Calendar mCalendar;

    @NonNull
    @Override
    public Dialog onCreateDialog(Bundle savedInstanceState) {
        Calendar date = Calendar.getInstance();
        int year = (mCalendar != null ? mCalendar : date).get(Calendar.YEAR);
        int month = (mCalendar != null ? mCalendar : date).get(Calendar.MONTH);
        int day = (mCalendar != null ? mCalendar : date).get(Calendar.DATE);

        DatePickerDialog dialog = new DatePickerDialog(getActivity(), mListener, year, month, day);
        dialog.getDatePicker().setMinDate(date.getTimeInMillis());
        return dialog;
    }

    public void setCalendar(Calendar mCalendar) {
        this.mCalendar = mCalendar;
    }

    public void setListener(DatePickerDialog.OnDateSetListener mListener) {
        this.mListener = mListener;
    }
}