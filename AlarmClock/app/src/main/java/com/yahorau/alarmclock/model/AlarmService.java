package com.yahorau.alarmclock.model;

import android.app.AlarmManager;
import android.app.PendingIntent;
import android.app.Service;
import android.content.Context;
import android.content.Intent;
import android.os.IBinder;
import com.yahorau.alarmclock.presenter.NotificationReceiver;
import java.util.Comparator;
import java.util.List;
import java.util.Set;
import java.util.TreeSet;

public class AlarmService extends Service {

    @Override
    public IBinder onBind(Intent intent) {
        return null;
    }

    @Override
    public void onCreate() {
        super.onCreate();
    }

    @Override
    public int onStartCommand(Intent intent, int flags, int startId) {
        Alarm alarm = getNext();
        if (alarm != null) {
            add(alarm, getApplicationContext());
        }
        return START_NOT_STICKY;
    }

    public static void add(Alarm alarm, Context context){
        Intent intent = new Intent(context, NotificationReceiver.class);
        intent.putExtra("alarm", alarm);

        PendingIntent pendingIntent = PendingIntent.getBroadcast(context, alarm.getId(), intent, PendingIntent.FLAG_CANCEL_CURRENT);
        AlarmManager alarmManager = (AlarmManager) context.getSystemService(Context.ALARM_SERVICE);
        alarmManager.setExact(AlarmManager.RTC_WAKEUP, alarm.getDate().getTimeInMillis(), pendingIntent);
    }

    public static void cancel(Alarm alarm, Context context){
        Intent myIntent = new Intent(context, NotificationReceiver.class);
        PendingIntent pendingIntent = PendingIntent.getBroadcast(context, alarm.getId(), myIntent, PendingIntent.FLAG_CANCEL_CURRENT);
        AlarmManager alarmManager = (AlarmManager) context.getSystemService(Context.ALARM_SERVICE);
        alarmManager.cancel(pendingIntent);
    }

    private Alarm getNext() {
        Set<Alarm> alarmQueue = new TreeSet<Alarm>(new Comparator<Alarm>() {
            @Override
            public int compare(Alarm lhs, Alarm rhs) {
                int result = 0;
                long diff = lhs.getDate().getTimeInMillis() - rhs.getDate().getTimeInMillis();
                if (diff > 0) {
                    return 1;
                } else if (diff < 0) {
                    return -1;
                }
                return result;
            }
        });

        DbHelper.init(getApplicationContext());
        List<Alarm> alarms = DbHelper.getAll();

        long currentTime = System.currentTimeMillis();
        for (Alarm alarm : alarms) {
            if (alarm.isActivated() && (alarm.getDate().getTimeInMillis() - currentTime > 0)) {
                alarmQueue.add(alarm);
            }
        }

        if (alarmQueue.iterator().hasNext()) {
            return alarmQueue.iterator().next();
        } else {
            return null;
        }
    }

    @Override
    public void onDestroy() {
        DbHelper.deactivate();
        super.onDestroy();
    }
}