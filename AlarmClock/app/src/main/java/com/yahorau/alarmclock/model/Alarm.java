package com.yahorau.alarmclock.model;

import com.yahorau.alarmclock.model.enums.AlarmType;
import com.yahorau.alarmclock.model.enums.AlarmDifficulty;
import java.io.Serializable;
import java.util.Calendar;

public class Alarm implements Serializable {
    private boolean isActivated = true;
    private String description = "";
    private Calendar date;
    private AlarmDifficulty alarmDifficulty = AlarmDifficulty.Medium;
    private AlarmType alarmType = AlarmType.Points;
    private int id = -1;

    public Alarm() {
        date = Calendar.getInstance();
        date.set(Calendar.SECOND, 0);
    }

    public Calendar getDate() {
        return date;
    }

    public void setDate(Calendar date) {
        this.date = date;
    }

    public AlarmDifficulty getAlarmDifficulty() {
        return alarmDifficulty;
    }

    public void setAlarmDifficulty(AlarmDifficulty alarmDifficulty) {
        this.alarmDifficulty = alarmDifficulty;
    }

    public AlarmType getAlarmType() {
        return alarmType;
    }

    public void setAlarmType(AlarmType alarmType) {
        this.alarmType = alarmType;
    }

    public String getDescription() {
        return description;
    }

    public boolean isActivated() {
        return isActivated;
    }

    public void setActivated(boolean activated) {
        isActivated = activated;
    }

    public void setDescription(String description) {
        this.description = description;
    }

    public int getId() {
        return id;
    }

    public void setId(int id) {
        this.id = id;
    }

    @Override
    public String toString() {
        return getDate().toString();
    }
}