package com.yahorau.alarmclock.view;

import android.os.Bundle;
import android.support.annotation.Nullable;
import android.support.v4.app.Fragment;
import android.support.v4.app.FragmentManager;
import android.support.v7.app.AppCompatActivity;
import android.support.v7.widget.Toolbar;

import com.yahorau.alarmclock.R;

import butterknife.Bind;
import butterknife.ButterKnife;

public class MainActivity extends AppCompatActivity {

    private final String SIMPLE_FRAGMENT_TAG = MainActivity.class.getSimpleName();
    private Fragment mFragment;
    private FragmentManager fragmentManager;

    @Bind(R.id.toolbar)
    Toolbar toolbar;

    @Override
    protected void onCreate(@Nullable Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);
        ButterKnife.bind(this);

        setSupportActionBar(toolbar);

        fragmentManager = getSupportFragmentManager();
        if (savedInstanceState == null) {
            startFragment();
        } else {
            fragmentManager
                    .beginTransaction()
                    .replace(R.id.content_main, fragmentManager.findFragmentById(R.id.content_main), SIMPLE_FRAGMENT_TAG)
                    .commit();
        }
        fragmentManager.addOnBackStackChangedListener(new FragmentManager.OnBackStackChangedListener() {
            @Override
            public void onBackStackChanged() {
                mFragment = fragmentManager.findFragmentById(R.id.content_main);
            }
        });
    }

    @Override
    protected void onStart() {
        super.onStart();
    }

    @Override
    protected void onPause() {
        super.onPause();
    }

    @Override
    protected void onResume() {
        super.onResume();
    }

    private void startFragment() {
        mFragment = new MainView();

        fragmentManager
                .beginTransaction()
                .replace(R.id.content_main, mFragment, SIMPLE_FRAGMENT_TAG)
                .commit();
    }

    @Override
    public void onBackPressed() {
        int count = getFragmentManager().getBackStackEntryCount();

        if (count == 0) {
            super.onBackPressed();
        } else {
            getFragmentManager().popBackStack();
        }
    }
}