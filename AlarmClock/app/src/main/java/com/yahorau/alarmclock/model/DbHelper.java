package com.yahorau.alarmclock.model;

import android.content.ContentValues;
import android.content.Context;
import android.database.Cursor;
import android.database.sqlite.SQLiteDatabase;
import android.database.sqlite.SQLiteOpenHelper;
import com.yahorau.alarmclock.model.enums.AlarmType;
import com.yahorau.alarmclock.model.enums.AlarmDifficulty;
import java.util.ArrayList;
import java.util.Calendar;
import java.util.Date;
import java.util.List;

public class DbHelper extends SQLiteOpenHelper {

	private static DbHelper instance = null;
	private static SQLiteDatabase database = null;

	private static final String DATABASE_NAME = "DB";
	private static final int DATABASE_VERSION = 2;

	private static final String TABLE = "alarms";
	private static final String COLUMN_ALARM_ID = "_id";
	private static final String COLUMN_ALARM_TIME = "time";
	private static final String COLUMN_ALARM_DESCRIPTION = "description";
	private static final String COLUMN_ALARM_DIFFICULTY = "difficulty";
	private static final String COLUMN_ALARM_TYPE = "type";
	private static final String COLUMN_ALARM_ACTIVE = "active";

	public static void init(Context context) {
		if (instance == null) {
			instance = new DbHelper(context);
		}
	}

	private static SQLiteDatabase getDatabase() {
		if (database == null) {
			database = instance.getWritableDatabase();
		}
		return database;
	}

	static void deactivate() {
		if (database !=null && database.isOpen()) {
			database.close();
		}
		database = null;
		instance = null;
	}

	public static long create(Alarm alarm) {
		ContentValues cv = new ContentValues();
		cv.put(COLUMN_ALARM_TIME, alarm.getDate().getTimeInMillis());
		cv.put(COLUMN_ALARM_DESCRIPTION, alarm.getDescription());
		cv.put(COLUMN_ALARM_DIFFICULTY, alarm.getAlarmDifficulty().ordinal());
		cv.put(COLUMN_ALARM_TYPE, alarm.getAlarmType().ordinal());
		cv.put(COLUMN_ALARM_ACTIVE, String.valueOf(alarm.isActivated()));

		return getDatabase().insert(TABLE, null, cv);
	}

	public static int update(Alarm alarm) {
		ContentValues cv = new ContentValues();
		cv.put(COLUMN_ALARM_ACTIVE, alarm.isActivated());

		return getDatabase().update(TABLE, cv, "_id=" + alarm.getId(), null);
	}

	public static Alarm getAlarm(int id) {

		String[] columns = new String[] {
				COLUMN_ALARM_ID,
				COLUMN_ALARM_TIME,
				COLUMN_ALARM_DESCRIPTION,
				COLUMN_ALARM_DIFFICULTY,
				COLUMN_ALARM_TYPE,
				COLUMN_ALARM_ACTIVE,
		};
		Cursor cursor = getDatabase().query(TABLE, columns, COLUMN_ALARM_ID+"="+id, null, null, null,
				null);
		Alarm alarm = null;

		if(cursor.moveToFirst()){
			alarm.setId(cursor.getInt(0));
			alarm.setDate(convertFromString(cursor.getLong(1)));
			alarm.setDescription(cursor.getString(2));
			alarm.setAlarmDifficulty(AlarmDifficulty.values()[cursor.getInt(3)]);
			alarm.setAlarmType(AlarmType.values()[cursor.getInt(4)]);
			alarm.setActivated(Boolean.valueOf(cursor.getString(5)));
		}
		cursor.close();
		return alarm;
	}

	private static Calendar convertFromString(long alarmMilis) {
		Calendar calendar = Calendar.getInstance();
		Date date = new Date();
		date.setTime(alarmMilis);
		calendar.setTime(date);
		return calendar;
	}

	private static Cursor getCursor() {
		String[] columns = new String[] {
				COLUMN_ALARM_ID,
				COLUMN_ALARM_TIME,
				COLUMN_ALARM_DESCRIPTION,
				COLUMN_ALARM_DIFFICULTY,
				COLUMN_ALARM_TYPE,
				COLUMN_ALARM_ACTIVE,
		};
		return getDatabase().query(TABLE, columns, null, null, null, null,
				null);
	}

	private DbHelper(Context context) {
		super(context, DATABASE_NAME, null, DATABASE_VERSION);
	}

	@Override
	public void onCreate(SQLiteDatabase db) {
		db.execSQL("CREATE TABLE IF NOT EXISTS " + TABLE + " ( "
				+ COLUMN_ALARM_ID + " INTEGER primary key autoincrement, "
				+ COLUMN_ALARM_TIME + " INTEGER NOT NULL, "
				+ COLUMN_ALARM_DESCRIPTION + " TEXT NOT NULL, "
				+ COLUMN_ALARM_DIFFICULTY + " INTEGER NOT NULL, "
				+ COLUMN_ALARM_TYPE + " INTEGER NOT NULL, "
				+ COLUMN_ALARM_ACTIVE + " TEXT NOT NULL)");
	}

	@Override
	public void onUpgrade(SQLiteDatabase db, int oldVersion, int newVersion) {
		db.execSQL("DROP TABLE IF EXISTS " + TABLE);
		onCreate(db);
	}

	public static int deleteAll(){
		return getDatabase().delete(TABLE, "1", null);
	}

	public static List<Alarm> getAll() {
		List<Alarm> alarms = new ArrayList<>();
		Cursor cursor = DbHelper.getCursor();
		if (cursor.moveToFirst()) {
			do {
				Alarm alarm = new Alarm();
				alarm.setId(cursor.getInt(0));
				alarm.setDate(convertFromString(cursor.getLong(1)));
				alarm.setDescription(cursor.getString(2));
				alarm.setAlarmDifficulty(AlarmDifficulty.values()[cursor.getInt(3)]);
				alarm.setAlarmType(AlarmType.values()[cursor.getInt(4)]);
				alarm.setActivated(Boolean.valueOf(cursor.getString(5)));

				alarms.add(alarm);
			} while (cursor.moveToNext());
		}
		cursor.close();
		return alarms;
	}
}