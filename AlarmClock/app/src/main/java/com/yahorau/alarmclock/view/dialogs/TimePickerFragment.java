package com.yahorau.alarmclock.view.dialogs;

import android.app.Dialog;
import android.app.TimePickerDialog;
import android.os.Bundle;
import android.support.annotation.NonNull;
import android.support.v4.app.DialogFragment;
import java.util.Calendar;

public class TimePickerFragment extends DialogFragment {

    private TimePickerDialog.OnTimeSetListener mListener;
    private Calendar mDate;

    @NonNull
    @Override
    public Dialog onCreateDialog(Bundle savedInstanceState) {
        super.onCreateDialog(savedInstanceState);
        Calendar time = Calendar.getInstance();
        int hour = (mDate != null ? mDate : time).get(Calendar.HOUR_OF_DAY);
        int minutes = (mDate != null ? mDate : time).get(Calendar.MINUTE);

        return new TimePickerDialog(getContext(), mListener, hour, minutes, true);
    }




    public void setListener(TimePickerDialog.OnTimeSetListener mListener) {
        this.mListener = mListener;
    }

    public void setDate(Calendar date) {
        this.mDate = date;
    }
}