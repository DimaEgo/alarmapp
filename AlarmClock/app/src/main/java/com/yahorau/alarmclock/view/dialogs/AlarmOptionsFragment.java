package com.yahorau.alarmclock.view.dialogs;

import android.os.Bundle;
import android.support.annotation.Nullable;
import android.support.v4.app.Fragment;
import android.support.v7.app.ActionBar;
import android.support.v7.app.AppCompatActivity;
import android.view.LayoutInflater;
import android.view.MenuItem;
import android.view.View;
import android.view.ViewGroup;
import android.widget.RadioButton;
import android.widget.RadioGroup;
import com.yahorau.alarmclock.R;
import com.yahorau.alarmclock.model.enums.AlarmDifficulty;
import com.yahorau.alarmclock.model.enums.AlarmType;
import butterknife.Bind;
import butterknife.ButterKnife;

public class AlarmOptionsFragment extends Fragment {
    public static final String TAG = AlarmOptionsFragment.class.toString();
    public static final String DIFFICULTY_ID = "DifficultyId";
    public static final String TYPE_ID = "TypeId";

    @Bind(R.id.rb_difficulty_1)
    public RadioButton rbDifficulty1;
    @Bind(R.id.rb_difficulty_2)
    public RadioButton rbDifficulty2;
    @Bind(R.id.rb_difficulty_3)
    public RadioButton rbDifficulty3;
    @Bind(R.id.rb_type_alarm_1)
    public RadioButton rbTypeAlarm1;
    @Bind(R.id.rb_type_alarm_2)
    public RadioButton rbTypeAlarm2;
    @Bind(R.id.rb_type_alarm_3)
    public RadioButton rbTypeAlarm3;
    @Bind(R.id.rg_difficulty)
    public RadioGroup rgDifficulty;
    @Bind(R.id.rg_type_alarm)
    public RadioGroup rgTypeAlarm;

    private IAlarmSettings listener;

    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setRetainInstance(true);
    }

    @Nullable
    @Override
    public View onCreateView(LayoutInflater inflater, @Nullable ViewGroup container, @Nullable Bundle savedInstanceState) {
        View v = inflater.inflate(R.layout.difficulty_choose, container, false);
        ButterKnife.bind(this, v);
        setHasOptionsMenu(true);
        return v;
    }

    @Override
    public void onActivityCreated(@Nullable Bundle savedInstanceState) {
        super.onActivityCreated(savedInstanceState);
        setActionBarTitle();
        setInitialValuesRadioGroups(savedInstanceState);
    }

    private void setInitialValuesRadioGroups(@Nullable Bundle savedInstanceState) {
        if (savedInstanceState != null) {
            rgDifficulty.check(savedInstanceState.getInt(DIFFICULTY_ID));
            rgTypeAlarm.check(savedInstanceState.getInt(TYPE_ID));
        } else {
            rgDifficulty.check(getButtonId(listener.getCurrentDifficulty()));
            rgTypeAlarm.check(getButtonId(listener.getCurrentType()));
        }
    }

    private void setActionBarTitle() {
        ActionBar supportActionBar = ((AppCompatActivity) getActivity()).getSupportActionBar();
        if (supportActionBar != null) {
            supportActionBar.setLogo(R.drawable.toolbar_logo_0_margin);
            supportActionBar.setTitle(getResources().getString(R.string.toolbar_title_alarm_settings));
            supportActionBar.setDisplayHomeAsUpEnabled(true);
        }
    }

    @Override
    public void onSaveInstanceState(Bundle outState) {
        super.onSaveInstanceState(outState);
        outState.putInt(DIFFICULTY_ID, rgDifficulty.getCheckedRadioButtonId());
        outState.putInt(TYPE_ID, rgTypeAlarm.getCheckedRadioButtonId());
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        switch (item.getItemId()) {
            case android.R.id.home:
                if (listener != null) {
                    listener.onSettingsChanged(
                            getDifficultyMode(rgDifficulty.getCheckedRadioButtonId()), getTypeAlarmOnMode(rgTypeAlarm.getCheckedRadioButtonId()));
                }
                getActivity().getSupportFragmentManager().popBackStack();
                return true;
        }

        return super.onOptionsItemSelected(item);
    }

    public void setListener(IAlarmSettings listener) {
        this.listener = listener;
    }

    public int getDifficultyMode(int radioButtonId) {
        switch (radioButtonId) {
            case (R.id.rb_difficulty_1):
                return 0;
            case (R.id.rb_difficulty_2):
                return 1;
            case (R.id.rb_difficulty_3):
                return 2;
            default:
                return 0;
        }
    }

    public int getTypeAlarmOnMode(int radioButtonId) {
        switch (radioButtonId) {
            case (R.id.rb_type_alarm_1):
                return 0;
            case (R.id.rb_type_alarm_2):
                return 1;
            case (R.id.rb_type_alarm_3):
                return 2;
            default:
                return 0;
        }
    }

    public int getButtonId(AlarmDifficulty difficulty) {
        switch (difficulty) {
            case Lite:
                return R.id.rb_difficulty_1;
            case Medium:
                return R.id.rb_difficulty_2;
            case Strong:
                return R.id.rb_difficulty_3;
            default:
                return 0;
        }
    }

    public int getButtonId(AlarmType type) {
        switch (type) {
            case Plus:
                return R.id.rb_type_alarm_1;
            case Points:
                return R.id.rb_type_alarm_2;
            case PlusAndPosints:
                return R.id.rb_type_alarm_3;
            default:
                return 0;
        }
    }


    public interface IAlarmSettings {
        void onSettingsChanged(int item, int item2);
        AlarmType getCurrentType();
        AlarmDifficulty getCurrentDifficulty();
    }
}
